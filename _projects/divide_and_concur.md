---
name: 'Divide and Concur'
link: https://github.com/Alex-CD/divide-and-concur
index: 2
---

A (very simplistic) multithreaded openGL game engine, built as a learning exercise, my first foray into C++ and OpenGL, for my dissertation.
