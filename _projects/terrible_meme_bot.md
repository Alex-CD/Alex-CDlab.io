---
name: 'terrible-meme-bot'
link: https://gitlab.com/Alex-CD/terrible-meme-bot
index: 0
---

My biggest solo project, a media and utility bot for Discord. Can play audio, and do other useful (and not useful) stuff.
Extensible, configurable, and maintainable, powered by Node.JS, Typescript and Discord.JS. Running on 8 discord servers and counting!
