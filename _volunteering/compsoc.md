---
name: Royal Holloway Computing Society Committee
role: Communications Officer (17-18) , Ambassador (18-19)
period: 'Sep 2017 - Jun 2019'
link: https://computingsociety.co.uk/
index: 1
---
