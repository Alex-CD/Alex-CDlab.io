---
name: 'Kerv'
role: 'Junior Software Engineer'
link: https://kerv.com/
period: 'June 2023 - Present'
index: 0
---

Full stack software engineering consultant.
