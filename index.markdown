---
layout: home
---

<div class="base">
  <div class="row splash-console">

    <!-- about -->
    {%- include about.html -%}

    <!-- jobs -->
    <div class="console-item">
      <div class="console-command">
        <span class="console-name">~$</span> cat jobs.md
      </div>
      <div class="console-output">
         {%- include jobs.html -%}
      </div>
    </div>




    <!-- projects -->
    <div class="console-item">
      <div class="console-command">
        <span class="console-name">~$</span> cat projects.md
      </div>
      <div class="console-output">
        {% include projects-list.html %}
      </div>
    </div>

    <!-- volunteering -->
    <div class="console-item">
      <div class="console-command">
        <span class="console-name">~$</span> cat volunteering.md
      </div>
      <div class="console-output">
        {% include volunteering-list.html %}
      </div>
    </div>

    <!-- flashing cursor -->
    <div class="console-item">
      <div class="console-command">
        <span class="console-name">~$</span><div class="cursor"></div>
      </div>
    </div>

  </div>
</div>
